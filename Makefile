#
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

# begin of customizable variables
bin_dir = /usr/local/bin
man_dir = /usr/local/share/man/man1
# ccount_dir must not end by '/'
ccount_dir = $(root)/counters
# end of customizable variables

version = \"1.0.0\"
root = $(realpath .)

bin = ccount
src = $(wildcard src/*.c)
headers = $(wildcard src/*.h)
obj = $(subst .c,.o,$(src))

scripts = $(wildcard test/*.sh)

man_src = $(wildcard man/*.1)
man_pages = $(subst .1,.1.gz,$(man_src))

wflags = -Wall -Wextra -Werror=return-local-addr -Werror=int-conversion
macros = -DVERSION=$(version) -DCCOUNT_DIR=\"$(ccount_dir)\"
debug_flags = -g3 -O0 --coverage
cflags = -c -std=c99 -pedantic
cc = cc $(cflags) $(debug_flags) $(macros) $(wflags)

.PHONY: clean all install uninstall cov

all: compile man

install: man compile
	mkdir -p $(ccount_dir)
	touch $(ccount_dir)/LAST
	$(foreach man_page,$(man_pages),cp -f $(man_page) $(man_dir);)
	cp -f $(bin) $(bin_dir)

uninstall:
	rm -rf $(bin_dir)/$(bin) $(ccount_dir)
	$(foreach man_page,$(man_pages), rm -f $(man_dir)/$(man_page);)

compile: $(bin)

$(bin): $(obj) $(headers)
	cc --coverage $(obj) -o $@

%.o: %.c
	$(cc) $< -o $@

man: $(man_pages)

$(man_pages): $(man_src)

%.1.gz: %.1
	gzip -9 -c $< > $@

clean:
	rm -f $(man_pages) $(bin) $(obj)
	rm -f $(subst .c,.gcno,$(src))
	rm -f $(subst .c,.gcda,$(src))
	rm -f $(subst .c,.gcov,$(src))

test: compile
	chmod +x test/*.sh
	touch counters/LAST
	@$(foreach script,$(scripts),$(script);)

cov: compile
	gcov src/*.gcno
