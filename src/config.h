/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#ifndef CONFIG_H
#define CONFIG_H

/* Maximum length of a counter name. Used for optimizing heap allocation. */
#define COUNTER_NAME_LENGTH 60

/* Length of the CCOUNT_DIR string. */
#define CCOUNT_DIR_LEN (sizeof(CCOUNT_DIR)/sizeof(char) - 1)

/* Maximum number of digits of an int. The number (2^64 - 1) has 20 digits, this
   value is safe. */
#define INT_LEN 20

#endif
