/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>

#include "io.h"
#include "config.h"

void
build_path(char *restrict dest, char const *restrict name)
{
	memcpy(dest, CCOUNT_DIR, CCOUNT_DIR_LEN);
	dest[CCOUNT_DIR_LEN] = '/';
	strcpy(dest + CCOUNT_DIR_LEN + 1, name);
}

int
counter_check(char const *name)
{
	char path[CCOUNT_DIR_LEN + 1 + COUNTER_NAME_LENGTH + 1];
	struct stat buf;
	int status;

	build_path(path, name);
	status = stat(path, &buf);

	if (status >= 0)
		return 1;

	if (errno == ENOENT)
		return 0;

	return -1;
}

int
counter_create(char const *name)
{
	int status;

	status = counter_write(name, 0);

	if (status >= 0)
		return 0;

	fprintf(stderr, "Failed to create counter %s.\n", name);

	return 4;
}

int
counter_create_soft(char const *name)
{
	int exists;

	exists = counter_check(name);

	if (exists > 0)
	{
		fprintf(stderr, "Counter %s already exists.\n", name);

		return 2;
	}

	if (exists == 0)
		return counter_create(name);

	return 4;
}

int
counter_create_force(char const *name)
{
	int exists;

	exists = counter_check(name);

	if (exists > 0)
		return 0;

	if (exists == 0)
		return counter_create(name);

	return 4;
}

int
counter_delete(char const *name)
{
	char path[CCOUNT_DIR_LEN + 1 + COUNTER_NAME_LENGTH + 1];
	int status;
	char *last;

	build_path(path, name);
	status = unlink(path);

	if (status < 0)
	{
		fprintf(stderr, "Failed to delete %s.\n", name);

		return 4;
	}

	last = read_last();

	if (last == NULL)
		return 4;

	if (strcmp(last, name) == 0)
	{
		status = erase_last();

		if (status >= 0)
		{
			free(last);

			return 0;
		}

		free(last);

		return 4;
	}

	free(last);

	return 0;
}

int
counter_delete_soft(char const *name)
{
	int exists;

	exists = counter_check(name);

	if (exists > 0)
		return counter_delete(name);

	if (exists == 0)
	{
		fprintf(stderr, "Counter %s does not exist.\n", name);

		return 2;
	}

	return 4;
}

int
counter_delete_force(char const *name)
{
	int exists;

	exists = counter_check(name);

	if (exists > 0)
		return counter_delete(name);

	if (exists == 0)
		return 0;

	return 4;
}

static void
warning_last()
{
		fprintf(stderr, "Warning : failed to update LAST.\n");
}

char *
read_last()
{
	FILE *stream;
	char *name = NULL;
	size_t size = 0;
	ssize_t read;

	stream = fopen(CCOUNT_DIR"/LAST", "r");

	if (stream == NULL)
	{
		warning_last();

		return NULL;
	}

	read = getline(&name, &size, stream);

	if (read > 0)
		name[read - 1] = '\0'; /* remove '\n' */

	fclose(stream);

	return name;
}

int
set_last(const char *name)
{
	FILE *stream;
	char *current;
	size_t size;

	stream = fopen(CCOUNT_DIR"/LAST", "r");

	if (stream == NULL)
	{
		warning_last();

		return -1;
	}

	current = malloc(sizeof(char)*(CCOUNT_DIR_LEN + 2));
	size = sizeof(char)*(CCOUNT_DIR_LEN + 2);

	getline(&current, &size, stream);

	if (strcmp(name, current) == 0)
		return 0;

	freopen(CCOUNT_DIR"/LAST", "w", stream);
	fprintf(stream, "%s\n", name);

	free(current);
	fclose(stream);

	return 0;
}

int
erase_last()
{
	FILE *stream;

	stream = fopen(CCOUNT_DIR"/LAST", "w");

	if (stream == NULL)
	{
		warning_last();

		return -1;
	}

	fclose(stream);

	return 0;
}

static void
error_read(char const *name)
{
	fprintf(stderr, "Failed to read the value of %s.\n", name);
}

static void
error_write(char const *name)
{
	fprintf(stderr, "Failed to write to %s.\n", name);
}

int
counter_read(char const *name)
{
	char path[CCOUNT_DIR_LEN + 1 + COUNTER_NAME_LENGTH + 1];
	size_t size = sizeof(char)*(INT_LEN + 1 + 1);
	ssize_t read;
	char *buf;
	char *err = NULL;
	int val;
	FILE *stream;

	build_path(path, name);
	stream = fopen(path, "r");

	if (stream == NULL)
	{
		error_read(name);

		return INT_MAX;
	}

	buf = malloc(sizeof(char)*size);
	read = getline(&buf, &size, stream);

	if (read <= 0)
	{
		error_read(name);

		fclose(stream);
		free(buf);

		return INT_MAX;
	}

	buf[read - 1] = '\0';	/* remove '\n' */
	val = strtol(buf, &err, 10);

	if ((err != NULL) && (*err != '\0'))
	{
		error_read(name);

		return INT_MAX;
	}

	free(buf);
	fclose(stream);

	return val;
}

int
counter_write(char const *name, int val)
{
	char path[CCOUNT_DIR_LEN + 1 + COUNTER_NAME_LENGTH + 1];
	FILE *stream;
	int status;

	build_path(path, name);
	stream = fopen(path, "w");

	if (stream == NULL)
	{
		error_write(name);

		return -1;
	}

	fprintf(stream, "%d\n", val);
	fclose(stream);

	status = set_last(name);

	if (status >= 0)
		return 0;

	return -1;
}
