/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <dirent.h>
#include <limits.h>

#include "args.h"
#include "config.h"
#include "io.h"
#include "set.h"
#include "list.h"

int
ccount_create(int argc, char *argv[])
{
	if (argc < 3)
	{
		fprintf(stderr, "Not enough arguments.\n");

		return 1;
	}

	struct option const create_opts[] = {
		{"force", no_argument, NULL, 'f'},
		{NULL,    no_argument, NULL, 0}
	};

	int (*creation_f)(char const *) = counter_create_soft;
	int opt;

	optind = 2;		/* start parsing after 'create' */

	while (1)
	{
		opt = getopt_long(argc, argv, "+f", create_opts, NULL);

		if (opt < 0)
			break;

		switch (opt)
		{
		case 'f':
			creation_f = counter_create_force;
			break;
		case '?':
			invalid_opt(argv);
			return 1;
		default:
			print_usage(stderr);
			return 1;
		}
	}

	if (argv[argc - 1][0] == '-')
	{
		fprintf(stderr, "Missing NAME.\n");
		print_usage(stderr);

		return 1;
	}

	if (strlen(argv[argc - 1]) > COUNTER_NAME_LENGTH)
	{
		name_too_long(argv[argc - 1]);

		return 3;
	}

	int status = creation_f(argv[argc - 1]);

	return status;
}

int
ccount_delete(int argc, char *argv[])
{
	if (argc < 3)
	{
		fprintf(stderr, "Not enough arguments.\n");

		return 1;
	}

	struct option const delete_opts[] = {
		{"force", no_argument, NULL, 'f'},
		{NULL,    no_argument, NULL, 0}
	};

	int (*deletion_f)(char const *) = counter_delete_soft;
	int opt;

	optind = 2;		/* start parsing after 'delete' */

	while (1)
	{
		opt = getopt_long(argc, argv, "+f", delete_opts, NULL);

		if (opt < 0)
			break;

		switch (opt)
		{
		case 'f':
			deletion_f = counter_delete_force;
			break;
		case '?':
			invalid_opt(argv);
			return 1;
		default:
			print_usage(stderr);
			return 1;
		}
	}

	if (argv[argc - 1][0] == '-')
	{
		fprintf(stderr, "Missing NAME.\n");
		print_usage(stderr);
		return 1;
	}

	if (strlen(argv[argc - 1]) > COUNTER_NAME_LENGTH)
	{
		name_too_long(argv[argc - 1]);

		return 3;
	}

	int status = deletion_f(argv[argc - 1]);

	return status;
}

int
main(int argc, char *argv[])
{
	opterr = 0;		/* dismiss getopt error message */

	struct option const ccount_opts[] = {
		{"help",         no_argument, NULL, 'h'},
		{"version",      no_argument, NULL, 'V'},
		{NULL,           no_argument, NULL, 0}
	};

	while (1)
	{
		int opt = getopt_long(argc, argv, "+Vh", ccount_opts, NULL);

		if (opt < 0)
			break;

		switch (opt)
		{
		case 'h':
			print_help();
			return 0;
		case 'V':
			puts(VERSION);
			return 0;
		case '?':
			invalid_opt(argv);
			return 1;
		default:
			print_usage(stderr);
			return 1;
		}
	}

	if (argc < 2)
	{
		print_usage(stderr);
		return 1;
	}

	if (strcmp(argv[1], "set") == 0)
	{
		return ccount_set(argc, argv);
	}
	else if (strcmp(argv[1], "list") == 0)
	{
		return ccount_list(argc, argv);
	}
	else if (strcmp(argv[1], "delete") == 0)
	{
		return ccount_delete(argc, argv);
	}
	else if (strcmp(argv[1], "create") == 0)
	{
		return ccount_create(argc, argv);
	}

	fprintf(stderr, "Unknown command %s.\n", argv[1]);
	print_usage(stderr);

	return 1;
}
