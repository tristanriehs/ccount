/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#include "list.h"
#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <limits.h>

#include "args.h"
#include "config.h"
#include "io.h"

static int
counter_read_soft(char const *name)
{
	int exists;

	exists = counter_check(name);

	if (exists > 0)
		return counter_read(name);

	return INT_MAX;
}

static int
counter_read_force(char const *name)
{
	int exists;

	exists = counter_check(name);

	if (exists > 0)
		return counter_read(name);

	if (exists == 0)
	{
		int status = counter_create(name);

		if (status < 0)
			return INT_MAX;

		return 0;
	}

	return INT_MAX;
}

static int
maybe_invalid_opt(int argc, char *argv[])
{
	if (optind - 1 < argc - 1)
	{
		invalid_opt(argv);
		return -1;
	}

	return 0;
}


int
ccount_set(int argc, char *argv[])
{
	struct option const set_opts[] = {
		{"force", no_argument, NULL, 'f'},
		{"print", no_argument, NULL, 'p'},
		{NULL,    no_argument, NULL, 0}
	};

	int (*read_f)(char const *name) = counter_read_soft;
	int print_flag = 0;
	int opt;

	optind = 2;		/* start parsing after 'set' */

	while(1)
	{
		opt = getopt_long(argc, argv, "+fp", set_opts, NULL);

		if (opt < 0)
			break;

		switch(opt)
		{
		case 'f':
			read_f = counter_read_force;
			break;
		case 'p':
			print_flag = 1;
			break;
		case '?':
			opt = maybe_invalid_opt(argc, argv);

			if (opt < 0)
				return 1;

			break;
		default:
			print_usage(stderr);
			return 1;
		}
	}

	char *name;
	char operator;
	int operand;
	char *err;
	int val;
	int new_val;
	int status;

	if (argc < 4)
	{
		fprintf(stderr, "Not enough arguments.\n");
		print_usage(stderr);

		return 1;
	}

	name = argv[argc - 2];

	if (name[0] == '-')
	{
		fprintf(stderr, "Invalid name : %s.\n", name);
		print_usage(stderr);

		return 1;
	}

	if (strcmp(name, "LAST") == 0)
	{
		name = read_last();

		if (strlen(name) == 0)
		{
			fprintf(stderr, "LAST is blank.\n");

			free(name);

			return 4;
		}
	}

	operator = argv[argc - 1][0];
	val = read_f(name);

	if (val == INT_MAX)
		return 2;

	operand = strtol(argv[argc - 1] + 1, &err, 10);

	if ((err != NULL) && (*err != '\0'))
	{
		fprintf(stderr, "Invalid operand : %s.", argv[argc - 1] + 1);
		print_usage(stderr);

		return 3;
	}

	switch(operator)
	{
	case '+':
		new_val = val + operand;
		status = counter_write(name, new_val);
		break;
	case '-':
		new_val = val - operand;
		status = counter_write(name, new_val);
		break;
	case '=':
		new_val = operand;
		status = counter_write(name, operand);
		break;
	default:
		fprintf(stderr, "Invalid operator : %c.", operator);
		print_usage(stderr);

		if (name != argv[argc - 2])
			free(name);

		return 3;
	}

	if (status < 0)
	{
		if (name != argv[argc - 2])
			free(name);

		return 4;
	}

	if (print_flag)
		printf("%d\n", new_val);

	if (name != argv[argc - 2])
		free(name);

	return 0;
}
