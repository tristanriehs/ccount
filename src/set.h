/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#ifndef SET_H
#define SET_H

int ccount_set(int argc, char *argv[]);

#endif
