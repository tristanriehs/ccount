/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#ifndef CREATE_H
#define CREATE_H

/* Except counter_read, every function returns 0 on success and -1 on
   failure. */

/* Write the full path name of the counter NAME in DEST. DEST has to be at least
   (CCOUNT_DIR_LEN + 1 + COUNTER_NAME_LENGTH + 1) chars long in order to fit
   both the '/' and the '\0'. */
void build_path(char *restrict dest, char const *restrict name);

/* Check if the counter called NAME exists. Returns 1 if it does, 0 if it does
   not and -1 if an error occured. */
int counter_check(char const *name);

/* Write 0 to the file called NAME in the ccount directory. */
int counter_create(char const *name);

/* Create a counter called NAME. Fail if it already exists. */
int counter_create_soft(char const *name);

/* Same as counter_create_soft but does not fail if one already exists. And the
   existing counter is not modified. */
int counter_create_force(char const *name);

/* Unlink the file associated to the counter called NAME. */
int counter_delete(char const *name);

/* Delete the counter called NAME. Fail if it does not exist. */
int counter_delete_soft(char const *name);

/* Same as counter_delete_soft but does not fail if it does not exist. */
int counter_delete_force(char const *name);

/* Return an heap-allocated string containing the name that LAST refers to.
   Returns NULL if an error occured. */
char *read_last();

/* Set the counter called NAME as LAST. */
int set_last(char const *name);

/* Erase the value of LAST. */
int erase_last();

/* Return the value of the counter called NAME. */
int counter_read(char const *name);

/* Set the counter NAME to VAL. */
int counter_write(char const *name, int val);

#endif
