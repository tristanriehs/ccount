/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#ifndef LIST_H
#define LIST_H

/* Call PRINT_F with each counter name and a buffer full of '\0' to write
   into. Its length is (COUNTER_NAME_LENGTH + 1 + INT_LEN + 1). PRINT_F has to
   return 0 on succes. The printing stops whenever a call to PRINT_F returns
   non-zero. */
int list_iter(int (*print_f)(char const *name, char *buf));

/* Print both the name and the value of the counter called NAME. */
int print_simple(char const *name, char *buf);

/* Print only the value of the counter called NAME. */
int print_values(char const *name, char *buf);

/* Print only the name of the counter NAME. */
int print_names(char const *name, char *buf);

int ccount_list(int argc, char *argv[]);

#endif
