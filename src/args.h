/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#ifndef ARGS_H
#define ARGS_H

#include <stdio.h>

#ifndef VERSION
#define VERSION "unknown version"
#endif

/* Print an error message when an invalid option is parsed. */
void invalid_opt(char *argv[]);

/* Print help message to stdout. */
void print_help();

/* Print usage message to OUTPUT. */
void print_usage(FILE *output);

/* Print an error message when NAME is too long. */
void name_too_long(char const *name);

#endif
