/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <dirent.h>
#include <limits.h>

#include "args.h"
#include "config.h"
#include "io.h"
#include "list.h"

int
list_iter(int (*print_f)(char const *name, char *buf))
{
	DIR* dir;
	struct dirent *elm;
	int status = 0;
	char buf[COUNTER_NAME_LENGTH + 1 + INT_LEN + 1] = {'\0'};

	dir = opendir(CCOUNT_DIR);

	if (dir == NULL)
		return 2;

	while(1)
	{
		elm = readdir(dir);

		if (elm == NULL)
			break;

		if ((elm->d_type == DT_REG) &&
		    (strcmp(elm->d_name, "LAST") != 0))
		{
			status = print_f(elm->d_name, buf);
		}

		if (status != 0)
			break;

		memset(buf, '\0',
		       sizeof(char)*(COUNTER_NAME_LENGTH + 1 + INT_LEN + 1));
	}

	closedir(dir);

	return status;
}

int
print_simple(char const *name, char *buf)
{
	size_t name_len;
	int val;

	val = counter_read(name);

	if (val == INT_MAX)
		return 2;

	name_len = strlen(name);
	memcpy(buf, name, name_len*sizeof(char));
	buf[name_len] = ' ';
	sprintf(buf + name_len + 1, "%d", val);

	puts(buf);

	return 0;
}

int
print_values(char const *name, char *buf)
{
	(void) buf;
	int val;

	val = counter_read(name);

	if (val == INT_MAX)
		return 2;

	printf("%d\n", val);

	return 0;
}

int
print_names(char const *name, char *buf)
{
	(void) buf;
	puts(name);

	return 0;
}

int
ccount_list(int argc, char *argv[])
{
	struct option const list_opts[] = {
		{"values", no_argument, NULL, 'v'},
		{"names", no_argument, NULL, 'n'},
		{NULL,     no_argument, NULL, 0}
	};

	int (*print_f)(char const *name, char *buf) = print_simple;
	int opt;

	optind = 2;		/* start parsing after 'list' */

	while(1)
	{
		opt = getopt_long(argc, argv, "vn", list_opts, NULL);

		if (opt < 0)
			break;

		switch(opt)
		{
		case 'v':
			print_f = print_values;
			break;
		case 'n':
			print_f = print_names;
			break;
		case '?':
			invalid_opt(argv);
			return 1;
		default:
			print_usage(stderr);
			return 1;
		}
	}

	return list_iter(print_f);
}
