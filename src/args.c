/**
 * Written by Tristan Riehs.
 * This file is part of ccount and is licensed under the MIT License.
 */

#include <stdio.h>
#include <getopt.h>

#include "args.h"
#include "config.h"
#include <string.h>

void
print_usage(FILE *output)
{
	fprintf(output, "Usage :\n");
	fprintf(output, "ccount [-h | --help] [-V | --version]\n");
	fprintf(output, "ccount create [-f | --force] NAME\n");
	fprintf(output, "ccount delete [-f | --force] NAME\n");
	fprintf(output, "ccount list [-v | --values]\n");
	fprintf(output, "ccount set [-f | --force] [-p | --print] NAME OP\n");
}

void
print_help()
{
	puts("ccount - lightweight tool for managing Custome COUNTers");
	puts("");
	print_usage(stdout);
	puts("");
	puts("ccount base directory for this build :");
	puts(CCOUNT_DIR);
	puts("");
	puts("Consult the manual pages for further information :");
	puts("man ccount");
	puts("");
	puts("ccount is distributed under the terms of the MIT License.");
}

void
invalid_opt(char *argv[])
{

	if (optopt != '\0')
		fprintf(stderr, "Invalid option -%c.\n", optopt);
	else
		fprintf(stderr, "Invalid option %s.\n", argv[optind - 1]);

	print_usage(stderr);
}

void
name_too_long(const char *name)
{
	fprintf(stderr, "Name too long (%zu): %s. Maximum authorized length is \
%d.", strlen(name), name, COUNTER_NAME_LENGTH);
}
