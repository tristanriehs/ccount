ccount
===

A lightweight tool for managing Custom COUNTers.

I created `ccount` to easily count the number of soft resets while shiny
hunting.

# Usage

## List

```shell
ccount list
```

List your counters.

## Create

``` shell
ccount create NAME
```

Create a new counter called `NAME`.

## Delete

``` shell
ccount delete NAME
```

Delete the counter `NAME`.

## Set

``` shell
ccount set NAME OP
```

Perform the operation `OP` on the counter called `NAME`.

# Installation

`ccount` does not support non-free platforms.

## Dependencies

- standard C library

## Build Dependencies

- GNU Make
- C compiler (`ccount` executable)
- `gzip` (manual pages)
- Coreutils : `cp`, `mkdir` (installation)

## Installation

To customize the installation, directly edit the Makefile.

To compile `ccount` executable, run :

``` shell
make
```

To install the executable and the manual pages, run (root privileges may be
required) :

``` shell
make install
```

# Uninstallation

The following command uninstalls `ccount`. Root privileges may be required. It
also deletes the `ccount` directory, `$HOME/.config/ccount` by default.

```shell
make uninstall
```
