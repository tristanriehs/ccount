#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

sed -i "s|ccount_dir = \$(root)/test|ccount_dir = $HOME/.config/ccount|g" Makefile
