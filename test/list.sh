#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

rm -rf counters/*

echo "1" > counters/test0
echo "test0" > counters/LAST

echo "basic listing works"

./ccount list > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "listing works with --names"

./ccount list --names > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

./ccount list -n> /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "listing works with --values"

./ccount list --values > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

./ccount list -v > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
