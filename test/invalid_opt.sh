#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

echo "error when invalid option"

echo "'ccount'"

./ccount -t 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

./ccount --t 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "'ccount create'"

./ccount create -t 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

./ccount create -t test 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "'ccount delete'"

./ccount delete -t 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

./ccount delete -t test 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "'ccount list'"

./ccount list -t 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "'ccount set'"

./ccount set -t 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "unknown command"

./ccount unknown -t test 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
