#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

echo "the --help option works"

./ccount -h > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

./ccount --help > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
