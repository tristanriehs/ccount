#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

echo "fail if counter does not exist"

rm -f counters/test
./ccount set test +1 2> /dev/null

if [ $? -ne 2 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "fail if the operation is invalid"

echo "0" > counters/test
./ccount set test *1 2> /dev/null

if [ $? -ne 3 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "+ works"

echo "0" > counters/test
./ccount set test +1

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

val=$(cat counters/test)

if [ "$val" != "1" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "- works"

echo "8" > counters/test
./ccount set test -1

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

val=$(cat counters/test)

if [ "$val" != "7" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "= works"

echo "4" > counters/test
./ccount set test =1

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

val=$(cat counters/test)

if [ "$val" != "1" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "the --force option works"

rm -f counters/test
./ccount set --force test +4 2> /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

val=$(cat counters/test)

if [ "$val" != "4" ]; then
    echo -e "\tFAILED"
    exit 1
fi

rm -f counters/test
./ccount set -f test +5 2> /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

val=$(cat counters/test)

if [ "$val" != "5" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "the --print option works"

echo "0" > counters/test
val=$(./ccount set --print test +4)

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

if [ "$val" != "4" ]; then
    echo -e "\tFAILED"
    exit 1
fi

rm -f counters/test
val=$(./ccount set -f -p test +5)

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

if [ "$val" != "5" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
