#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

echo "every command fails if the access is denied"
chmod -x counters

echo "'create'"

./ccount create test

if [ $? -ne 4 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "'delete'"

./ccount delete test

if [ $? -ne 4 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

chmod +x counters
