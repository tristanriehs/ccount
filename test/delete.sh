#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

echo "deleting an existing counter works"

touch counters/test
./ccount delete test

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

if [ -e "counters/test" ]; then
    echo -e "\tFAILED"
    exit 1
fi

touch counters/test
./ccount delete -f test

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

if [ -e "counters/test" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "deleting a counter that does not exist with no force flag fails"

rm -f counters/test
./ccount delete test 2> /dev/null

if [ $? -ne 2 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "deleting a counter that does not exist with the force flag does not fail"

rm -f counters/test
./ccount delete -f test

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "deleting a counter with a too long name fails"

./ccount delete "too loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong" 2> /dev/null

if [ $? -ne 3 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
