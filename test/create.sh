#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

rm -f counters/test

echo "creating a counter called 'test' works"

./ccount create test

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

if [ ! -e "counters/test" ]; then
    echo -e "\tFAILED"
    exit 1
fi

rm -f counters/test
./ccount create -f test

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

if [ ! -e "counters/test" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "creating a counter that already exists with no force flag fails"

if [ ! -e "counters/test" ]; then
    touch "counters/test"
fi

./ccount create test 2> /dev/null

if [ $? -ne 2 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "creating a counter that already exists with the force flag does not fail"

echo "value" > counters/test
./ccount create -f test

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

if [ $(cat counters/test) != "value" ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "creating a counter with a too long name fails"

./ccount create "too looooooooooooooooooooooooooooo\
ooooooooooooooooooooooooooooooong" 2> /dev/null

if [ $? -ne 3 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
