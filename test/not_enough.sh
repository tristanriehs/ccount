#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

echo "error when no arguments"

./ccount 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "error when no argument for 'create'"

./ccount create 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "error when no NAME for create"

./ccount create -f 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "error when no argument for 'delete'"

./ccount delete 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "error when no NAME for delete"

./ccount delete -f 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"

echo "error when not enough arguments for 'set'"

./ccount set test 2> /dev/null

if [ $? -ne 1 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
