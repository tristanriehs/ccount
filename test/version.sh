#!/bin/sh
# Written by Tristan Riehs.
# This file is part of ccount and is licensed under the MIT License.
#

echo "the --version option works"

./ccount -V > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

./ccount --version > /dev/null

if [ $? -ne 0 ]; then
    echo -e "\tFAILED"
    exit 1
fi

echo -e "\tOK"
